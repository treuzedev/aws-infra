#####
# GENERAL CONFIGS
#####

#####
provider "aws" {
  region = "eu-west-1"
}

#####
terraform {

  required_version = "~> 0.15.1"

  backend "s3" {
    bucket = "treuzedev-terraform"
    key    = "justbytes/infra/state"
    region = "eu-west-1"
  }

}


#####
# TREUZE
#####

# vpc resources
# defaults provided by the module
module "main_vpc" {

  source      = "./modules/vpc"
  common_tags = var.treuze_tags

  subnets = var.main_vpc_subnets
  rts     = var.main_vpc_rts
  sgs     = var.main_vpc_sgs

}


#####
# JUSTBYTES
#####

# cdn distribution for js justbytes files
module "cdn_js_justbytes" {
  source = "./modules/cdn"
  common_tags = var.justbytes_tags
}
