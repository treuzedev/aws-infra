#####
variable "treuze_tags" {

  description = <<EOT
    Treuze common tags.\n
    Defaults to:
    {
      "Env" : "Production",
      "Owner" : "EMU",
      "App" : "Treuze",
    }
    EOT

  default = {
    "Env" : "Production",
    "Owner" : "EMU",
    "App" : "Treuze",
  }

}

#####
variable "justbytes_tags" {

  description = <<EOT
    JustBytes common tags.\n
    Defaults to:
    {
      "Env" : "Production",
      "Owner" : "EMU",
      "App" : "JustBytes",
    }
    EOT

  default = {
    "Env" : "Production",
    "Owner" : "EMU",
    "App" : "JustBytes",
  }

}
