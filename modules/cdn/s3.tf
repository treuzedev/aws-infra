#####
resource "aws_s3_bucket" "cdn_bucket" {

  bucket = "justbytes-cdn"
  acl    = "private"

  versioning {
    enabled = true
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
  
  lifecycle {
    prevent_destroy = false
  }
  
  force_destroy = true

  tags = var.common_tags

}


#####
resource "aws_s3_bucket_policy" "public-access" {
  
  bucket = aws_s3_bucket.cdn_bucket.id
  
  policy = jsonencode({
    Version = "2012-10-17",
    Id = "cdnBucketPublicAccess",
    Statement = [
      {
        Sid = "cdnAccess",
        Effect = "Allow",
        Principal = {
          "AWS": "arn:aws:iam::cloudfront:user/CloudFront Origin Access Identity ${aws_cloudfront_origin_access_identity.oai_cdn.id}",
        },
        Action = "s3:GetObject",
        Resource = [
          "${aws_s3_bucket.cdn_bucket.arn}/*",
        ],
      },
    ],
  })
  
}
