# VPC

A module to define VPC resources like the VPC itself, Subnets, SGs, etc..

<br>

## Variables

See the `vars.tf` in this module or the `vpc-vars.tf` in the root of the repo for more information and examples about the variables and their usage.

<br>

| Name | Description |
| --- | --- |
| `var.common_tags` | Common tags applied to all resources. No defaults. |
| `var.enable_dns_support` | Enable DNS support on a VPC. Defaults to true |
| `var.enable_dns_hostnames` | Enable DNS hostnames on a VPC. Defaults to true |
| `var.subnets` | A map, containing a another map for each key, where the key is the subnet name and the value a map configuring the subnet. No defaults |
| `var.rts` | A map, containing maps that configure a route table. No defaults. |
| `var.sgs` | A map, containing maps configuring a security group. No defaults. |
