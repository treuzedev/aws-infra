#####
# VPC
#####

#####
variable "common_tags" {
    description = "VPC module common tags. No defaults."
}

#####
variable "enable_dns_support" {
    description = "Enable DNS support on a VPC. Defaults to true"
    default = true
}

#####
variable "enable_dns_hostnames" {
    description = "Enable DNS hostnames on a VPC. Defaults to true"
    default = true
}

#####
variable "subnets" {
    description = "A map, containing a another map for each key, where the key is the subnet name and the value a map configuring the subnet. No defaults."
}

#####
variable "rts" {
    description = "A map, containing maps that configure a route table. No defaults."
}

#####
variable "sgs" {
    description = "A map, containing maps configuring a security group. No defaults."
}