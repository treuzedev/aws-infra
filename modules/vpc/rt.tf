#####
resource "aws_route_table" "main" {

    for_each = var.rts

    vpc_id = aws_vpc.main.id

    dynamic "route" {
        for_each = each.value.routes
        content {
            cidr_block = route.value.cidr
            gateway_id = route.value.type == "igw" ? aws_internet_gateway.main.id : ""
            # nat_gateway_id = route.type == "nat" ? aws_nat_gateway.main.id : ""
        }
    }

    tags = var.common_tags

}


#####
resource "aws_route_table_association" "main" {

    for_each = var.subnets

    subnet_id = aws_subnet.main[each.key].id
    route_table_id = aws_route_table.main[each.value.rt_name].id

}